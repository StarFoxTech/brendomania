﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

public class BrendToolsEditor : EditorWindow 
{
//	public List<BrendItem> Brends
//	{
//		get
//		{
//			if(_brends == null) _brends
//		}
//	}

	private List<BrendItem> _brends;

	[MenuItem("Tools/Brends")]
	public static void Init()
	{
		// получение или создание окна редактора брендов
		BrendToolsEditor target = EditorWindow.GetWindow<BrendToolsEditor>();

		target.title = "Brends Editor";

		target.minSize = new Vector2(512, 256);

		target.Show();
	}

	BrendItem testItem = new BrendItem { ImageURL = "123/123/123" };

	public void OnGUI()
	{
		try
		{
			EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);

			if(GUILayout.Button("Save", EditorStyles.toolbarButton, GUILayout.MaxWidth(32f))) BrendDatabase.Save();
			if(GUILayout.Button("Load", EditorStyles.toolbarButton, GUILayout.MaxWidth(32f))) BrendDatabase.Load();

			EditorGUILayout.Space();

			EditorGUILayout.LabelField("Count: " + BrendDatabase.Brends.Count, EditorStyles.miniLabel, GUILayout.MaxWidth(48f));

			if(GUILayout.Button("Add", EditorStyles.toolbarButton, GUILayout.MaxWidth(31f))) BrendDatabase.Brends.Add(new BrendItem());

			EditorGUILayout.EndHorizontal();

			BrendDatabase.Brends.ForEach(a => DrawItem(a));
		}
		catch(NullReferenceException)
		{
			BrendDatabase.Load();
		}
	}

	public void DrawItem(BrendItem item)
	{
		item.m_Foldout = EditorGUILayout.Foldout(item.m_Foldout, "URL: " + item.ImageURL);

		if(item.m_Foldout)
		{
			EditorGUILayout.BeginVertical(GUI.skin.box);

			item.ImageURL = EditorGUILayout.TextField("URL of Image", item.ImageURL);

			EditorGUILayout.BeginVertical(GUI.skin.box);

			EditorGUILayout.LabelField("Aliases", EditorStyles.boldLabel);
			item.Aliases.ForEach(a => DrawAlias(item, a));

			EditorGUILayout.Space();

			if(GUILayout.Button("Add")) item.Aliases.Add(new BrendAliasItem());

			EditorGUILayout.EndVertical();
			
			if(GUILayout.Button("Delete")) BrendDatabase.Brends.Remove(item);

			EditorGUILayout.EndVertical();
		}
	}

	public void DrawAlias(BrendItem brend, BrendAliasItem item)
	{
		EditorGUILayout.BeginHorizontal(GUI.skin.box);

		item.Value = EditorGUILayout.TextField(item.Value);

		if(GUILayout.Button("Delete", GUILayout.MaxWidth(128f)))
		{
			brend.Aliases.Remove(item);
		}

		EditorGUILayout.EndHorizontal();
	}
}
