﻿using UnityEngine;
using System;
using System.IO;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;

public class BrendDatabase : MonoBehaviour 
{
	public static readonly string PATH = "Data/Brends.collection"; // .collection -- типо формат для мажоров :D 

	public static List<BrendItem> Brends
	{
		get
		{
			if(_brends == null) Load();

			return _brends;
		}
		set
		{
			_brends = value;

			Save();
		}
	}

	private static List<BrendItem> _brends;

	static XmlSerializer serializer = new XmlSerializer(typeof(List<BrendItem>));

	void Awake()
	{

	}

	public static void Load()
	{
		if(!File.Exists(PATH))
		{
			Save(new List<BrendItem>());
		}

		using(StreamReader sr = new StreamReader(PATH))
		{
			_brends = serializer.Deserialize(sr) as List<BrendItem>;
		}
	}

	public static void Save() { Save (_brends); }

	public static void Save(List<BrendItem> obj)
	{
		using(StreamWriter sw = new StreamWriter(PATH))
		{
			serializer.Serialize(sw, obj);
		}
	}
}
