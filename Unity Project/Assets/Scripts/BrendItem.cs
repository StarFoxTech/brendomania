﻿using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;

public class BrendItem
{
	/// <summary>
	/// The URL to image
	/// </summary>
	public string ImageURL = "image.jpeg";

	/// <summary>
	/// The valuesx
	/// </summary>
	public List<BrendAliasItem> Aliases = new List<BrendAliasItem>();

#if UNITY_EDITOR
	[XmlIgnore]
	public bool m_Foldout = false;
#endif
}
